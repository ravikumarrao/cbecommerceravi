import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../notification.service';

type TLoginForm = {
  username: string;
  password: string;
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  hide: boolean;
  constructor(
    private http: HttpClient,
    private router: Router,
    private notification: NotificationService
  ) {}
  private loginUrl =
    'https://29z4xk3dxg.execute-api.ap-south-1.amazonaws.com/userLogin/login';

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required]),
    });
  }

  get username() {
    return this.loginForm.get('username')!;
  }
  get password() {
    return this.loginForm.get('password')!;
  }

  async onLogin(userDetails: TLoginForm) {
    try {
      const response = await this.http
        .post(this.loginUrl, userDetails)
        .toPromise();
      if (!response) {
        this.notification.showError('User not found');
      }
      localStorage.setItem('username', userDetails.username);
      this.notification.showSuccess((response as { message: string }).message);
      this.router.navigate(['/products']);
    } catch (error) {
      this.notification.showError(
        (error as { error: { message: string } }).error.message
      );
    }
  }
}
