import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (localStorage.getItem('username')) {
      if (state.url === '/login') {
        this.router.navigate(['/products']);
        return false;
      }
      if (state.url === '/products') {
        return true;
      }
    } else {
      if (state.url === '/products') {
        this.router.navigate(['/login']);
        return false;
      }
    }
    return true;
  }
}
