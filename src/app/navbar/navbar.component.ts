import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from '../notification.service';

export interface MenuItem {
  label: string;
  icon: string;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  constructor(
    private router: Router,
    private notification: NotificationService
  ) {}
  menuItems: MenuItem[] = [
    {
      label: 'Home',
      icon: '',
    },
    {
      label: 'Logout',
      icon: '',
    },
    {
      label: '',
      icon: 'email',
    },
    {
      label: '',
      icon: 'notifications',
    },
    {
      label: 'Ravi',
      icon: 'account_circle',
    },
  ];
  ngOnInit(): void {}
  onLogout(label: string) {
    if (label === 'Logout') {
      localStorage.clear();
      this.router.navigate(['/login']);
      this.notification.showSuccess('Logged out successfully');
    }
  }
}
