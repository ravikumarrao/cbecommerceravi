import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../notification.service';
import { ProductsService } from '../products.service';

type TItems = {
  category: string;
  description: string;
  id: number;
  image: string;
  price: number;
  rating: { rate: number; count: number };
  title: string;
};

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  cards: TItems[];

  constructor(
    private products: ProductsService,
    private notification: NotificationService
  ) {}

  async getProducts() {
    const response = await this.products.getProducts();
    if (!response) {
      this.notification.showError('Unable to load products');
    }
    this.cards = response as TItems[];
  }

  ngOnInit(): void {
    this.getProducts();
  }
}
